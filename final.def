

\fancypagestyle{inggrid}{%
	\fancyhf{}% clear all header and footer fields
	\fancyhead[L]{\normalsize\sffamily\MakeUppercase{\@articletype}}
	\fancyhead[R]{\sffamily\printfield{shorttitle}{}{}}
	\fancyfoot[L]{\sffamily ing.grid, \the\year, \getfield{doi}{}}
	\fancyfoot[R]{\sffamily \thepage}
	\renewcommand{\headrulewidth}{0pt}%
	\renewcommand{\footrulewidth}{0pt}%
}

\def\@maketitle{%
	\newpage \null%
    \begin{whitebox}
		\begin{flushleft}%
			\normalsize\sffamily%
			{\normalsize\MakeUppercase{\@articletype}\par}
			\marginnote{\includegraphics[width=\linewidth]{./logos/inggrid-preprints.pdf}}%[.5\baselineskip]
			{\Large\bfseries \@title \par}%
			{\normalsize\bfseries\@subtitle\par}%
			{\vskip 1.5em%
				\normalsize\printauthors
				\vskip 1.5em%
					{\footnotesize\printaffiliations}
				\vskip 1em%
			}
		\end{flushleft}%
	\end{whitebox}
	\marginpar{% OpenAccess Logo
		\vskip 2ex
		\adjustbox{width=2em}{\textcolor{BurntOrange}{\aiOpenAccess}}%
		\vskip 2ex
	}
    \sideinfofield{Date Submitted}{datesubmitted}
    \sideinfofield{Licenses}{licenses}
    \sideinfofield{Keywords}{keywords}
    \sideinfofield{Data availability}{dataavailability}
    \sideinfofield{Software availability}{softwareavailability}
    \sideinfos%
}
